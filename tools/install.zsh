#!/usr/bin/env zsh
banner=(
'   _________________'
'  / ___/_  __/_  __/'
'  \__ \ / /   / /   '
' ___/ // /   / /    '
'/____//_/   /_/     '
'                    '              
""
)
version="latest"

i18n=($(echo $LANG | tr "." "\n"))
if test ${i18n[2]} != "UTF-8"; then
    echo "You must use UTF-8."
    exit 1
fi
l10n=($(echo ${i18n[1]} | tr "_" "\n"))
lang=${l10n[1]}
loc=${l10n[2]}

if [ $lang = "fr" ]; then
    GIT_MODEL="https://gitlab.com/waser-technologies/models/fr/deepspeech.git"
    MODEL_LANGUAGE="french"
else
    GIT_MODEL="https://gitlab.com/waser-technologies/models/en/deepspeech.git"
    MODEL_LANGUAGE="english"
fi

print_version() {
    printf "%*s\n" $(( (${#version} + 4 + ${COLUMNS}) / 2))  "[ $version ]"
    echo -e "\n\n"
}

print_banner() {
    echo -e "\n\n\n"
    for i in ${banner}; do
        printf "%*s\n" $(( (${#i} + ${COLUMNS}) / 2)) "$i"
    done
    echo -e "\n"
    print_version
}


check_for_arch() {
    clear
    print_banner
    echo -e "\n"
    if [ -f "/etc/arch-release" ]; then
        release=$(cat '/etc/arch-release')
        echo "BTW, I use ${release}."
        sleep 0.5
    else
        echo "Assistant: You should use Arch Linux."
        exit 1
    fi
    echo -e "\n"
}

check_for_connectivity() {
    clear
    print_banner
    echo -e "\n"
    if ping -c 1 8.8.8.8 >/dev/null 2>&1; then
        echo "I'll just borrow your Internet connection for a while."
        sleep 0.5
    else
        echo "Assistant: Sorry Pal."
        echo "Assistant: You need Internet to install anything theses days."
        exit 2
    fi
}



print_banner
check_for_arch
check_for_connectivity

. <(curl -sLo- "https://git.io/progressbar")

bar::start

StuffToDo=(
    "echo 'Preparing speech recognition services'"
    "sleep 2 && echo 'Don´t worry it´s not painful'"
    "sleep 1 && echo 'It just allows me to understand you better'"
    "sleep 1 && echo 'Won´t be long I promise'"
    "sleep 1 && echo 'I´ll start downloading the files'"
    "sleep 1 && echo 'Downloading files'"
    "sleep 1 && echo 'Downloading STT'"
    "git clone https://gitlab.com/waser-technologies/technologies/stt.git /tmp/stt"
    "echo 'Downloading ${MODEL_LANGUAGE} model for STT'"
    "git clone ${GIT_MODEL} /tmp/stt/models"
    "echo 'Downloading dependencies'"
    "pip install -r /tmp/stt/requirements.txt"
    "echo 'Fix PyAudio for Python 3.10'"
    "pip install git+https://gitlab.com/waser-technologies/technologies/pyaudio.git"
    "echo 'Copying files'"
    "sleep 1 && sudo cp -f /tmp/stt/ear /usr/bin/ear && echo 'Copying files (1/7) [.     ]'"
    "sleep 1 && sudo cp -f /tmp/stt/ear.service /etc/systemd/user/ear.service && echo 'Copying files (2/7) [..     ]'"
    "sleep 1 && mkdir -p ~/.assistant/helpers/ && cp -f /tmp/stt/data.helper ~/.assistant/helpers/deepspeech.helper && echo 'Copying files (3/7) [...    ]'"
    "sleep 1 && mkdir -p ~/.assistant/trainers/ && cp -f /tmp/stt/models/model.train ~/.assistant/trainers/stt.train && echo 'Copying files (4/7) [....   ]'"
    "sleep 1 && mkdir -p ~/.assistant/models/$lang/SR && cp -f /tmp/stt/models/*.tflite ~/.assistant/models/$lang/SR/ && echo 'Copying files (5/7) [.....  ]'"
    "sleep 1 && cp -f /tmp/stt/models/*.scorer ~/.assistant/models/$lang/SR/ && echo 'Copying files (6/7) [...... ]'"
    "sleep 1 && cp -f /tmp/stt/models/alphabet.txt ~/.assistant/models/$lang/SR/ && echo 'Copying files (7/7) [.......]'"
    "sleep 1 && echo 'Enabling speech recognition services'"
    "sleep 1 && sudo systemctl enable --now ear"
    "echo 'Removing old files'"
    "sleep 1 && trash -rf /tmp/stt/"
    "echo 'Installation complete'"
)

TotalSteps=${#StuffToDo[@]}

for Stuff in ${StuffToDo[@]}; do
    out=$(tail -c 47 <<< $(eval "${Stuff}"))
    sleep 5
    clear
    print_banner    
    printf "%*s\n" $(( (${#out} + ${COLUMNS}) / 2)) "$out"
    StepsDone=$((${StepsDone:-0}+1))
    bar::status_changed $StepsDone $TotalSteps
done

bar::stop
